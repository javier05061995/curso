/** @format */

import React, { Component } from "react";

class Reloj extends Component {
  componentWillUnmount() {
    console.log(3, "El componente ha sido eliminado del DOM");
  }
  render() {
    return (
      <div>
        <h3>{this.props.hora}</h3>
      </div>
    );
  }
}
export default class CicloVida extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hora: new Date().toLocaleTimeString(),
      visible: false,
    };
  }
  componentDidMount() {}
  componentDidUpdate(prevProps, prevState) {
    console.log(prevProps);
    console.log(prevState);
  }

  tictoc = () => {
    this.temporizador = setInterval(() => {
      this.setState({
        hora: new Date().toLocaleTimeString(),
      });
    }, 1000);
  };

  iniciar = () => {
    this.tictoc();
    this.setState({
      visible: true,
    });
  };
  detener = () => {
    clearInterval(this.temporizador);
    this.setState({
      visible: false,
    });
  };

  render() {
    return (
      <div>
        <h2>Ciclo de vidad d elos componet de clase</h2>
        {this.state.visible && <Reloj hora={this.state.hora} />}
        <button onClick={this.iniciar}>iniciar</button>
        <button onClick={this.detener}>detener</button>
      </div>
    );
  }
}
