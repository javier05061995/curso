/** @format */
import React from "react";
import { useFecth } from "../hooks/useFecth";
function HooksPersonalisado() {
  let url = "https://pokeapi.co/api/v2/pokemon/";
  url = "https://jsonplaceholder.typicode.com/users";
  let { data, pending, error } = useFecth(url);
  return (
    <>
      <h2>Hook Personalisado</h2>
      <h3>{JSON.stringify(pending)}</h3>
      <h3>
        <mark></mark>
      </h3>
      <h3>
        <mark>{JSON.stringify(error)}</mark>
      </h3>

      <h3>
        <pre style={{ whiteSpace: "pre-wrap" }}>
          <code>{JSON.stringify(data)}</code>
        </pre>
      </h3>
    </>
  );
}

export default HooksPersonalisado;
