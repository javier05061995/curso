import React from "react";
import styled from "styled-components";

function ComponenteEstilisados(props) {
  const Title = styled.h2`
    font-size: 1.5em;
    text-align: center;
    color: palevioletred;
  `;

  // Create a Wrapper component that'll render a <section> tag with some styles
  const Wrapper = styled.section`
    padding: 1em;
    background: papayawhip;
  `;

  return (
    <Wrapper>
      <Title>Hello World!</Title>
    </Wrapper>
  );
}

export default ComponenteEstilisados;
