/** @format */

import React from "react";
import PropTypes from "prop-types";

const Propiedades = (props) => {
  return (
    <div>
      <h2> {props.porDefecto}</h2>
      <ul>
        <li>{props.cadena}</li>
        <li>{props.numero}</li>
        <li>{props.boleano ? "Verdadero" : "falso"}</li>
        <li>{props.arreglo.join(", ")} </li>
        <li>{props.objeto.nombre} </li>
        <li>{props.objeto.correo} </li>
        <li>{props.arreglo.map(props.funtion).join(", ")} </li>
        <li>{props.elementoReact} </li>
        <li>{props.componenteReact} </li>
      </ul>
    </div>
  );
};

Propiedades.defaultProps = {
  porDefecto: "las Props",
};

Propiedades.propTypes = {
  // You can declare that a prop is a specific JS primitive. By default, these
  // are all optional.
  //   optionalArray: PropTypes.array,
  //   optionalBigInt: PropTypes.bigint,
  //   optionalBool: PropTypes.bool,
  //   optionalFunc: PropTypes.func,
  numero: PropTypes.number.isRequired,
  //   optionalObject: PropTypes.object,
  //   optionalString: PropTypes.string,
  //   optionalSymbol: PropTypes.symbol,
};

export default Propiedades;
