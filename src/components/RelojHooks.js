import React, { useState, useEffect } from "react";

function Reloj(props) {
  return (
    <>
      <h3>{props.hora}</h3>
    </>
  );
}
export default function RelojHooks() {
  const [time, setTime] = useState(new Date().toLocaleTimeString());
  const [visible, setVisible] = useState(false);
  useEffect(() => {
    let temporizador;
    if (visible) {
      temporizador = setInterval(() => {
        setTime(new Date().toLocaleTimeString());
      }, 1000);
    } else {
      clearInterval(temporizador);
      return () => {
        console.log("fase de demontaje");
        clearInterval(temporizador);
      };
    }
  }, [visible]);
  return (
    <>
      <h2> reloj con hooks</h2>
      {visible && <Reloj hora={time} />}
      <button onClick={() => setVisible(true)}>iniciar</button>
      <button onClick={() => setVisible(false)}>detener</button>
    </>
  );
}
