/** @format */

import React, { Component } from "react";

export class EventosES6 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contador: 0,
    };
    this.sumar = this.sumar.bind(this);
    this.restar = this.restar.bind(this);
  }

  sumar() {
    this.setState({
      contador: this.state.contador + 1,
    });
  }
  restar() {
    this.setState({
      contador: this.state.contador - 1,
    });
  }
  render() {
    return (
      <div>
        <h2> Eventos Componentes ES6</h2>
        <nav>
          <button onClick={this.sumar}>+</button>
          <button onClick={this.restar}>-</button>
        </nav>
        <h3> {this.state.contador}</h3>
      </div>
    );
  }
}

//  Property Initializers
//arrow fuction
export class EventosES7 extends Component {
  state = {
    contador: 0,
  };

  sumar = () => {
    this.setState({
      contador: this.state.contador + 1,
    });
  };
  restar = () => {
    this.setState({
      contador: this.state.contador - 1,
    });
  };
  render() {
    return (
      <div>
        <h2> Eventos Componentes ES7</h2>
        <nav>
          <button onClick={this.sumar}>+ </button>
          <button onClick={this.restar}>-</button>
        </nav>
        <h3> {this.state.contador}</h3>
      </div>
    );
  }
}

export class MasSobreEventos extends Component {
  handleClick = (e, mensaje) => {
    console.log(e);
    console.log(e.nativeEvent);
    console.log(e.target);
    console.log(mensaje);
  };
  render() {
    return (
      <div>
        <h2>Mas sobre Eventos</h2>
        <button onClick={this.handleClick}>Saludar</button>
        {/*evento personalizado* */}
        <Boton MyOnClick={(e) => this.handleClick(e, "hola")} />
      </div>
    );
  }
}

// function Boton(props) {
//   return <button onClick={props.MyOnClick}>Boton heco componente</button>;
// }
// const Boton = (props) => (
//   <button onClick={props.MyOnClick}>Boton heco componente</button>
// );
const Boton = ({ MyOnClick }) => (
  <button onClick={MyOnClick}>Boton heco componente</button>
);
