import React, { useState, useEffect } from "react";

export default function CicloVida() {
  const [scroollY, setScroollY] = useState(0);
  useEffect(() => {
    const destectarScroll = () => setScroollY(window.pageYOffset);
    window.addEventListener("scroll", destectarScroll);

    return () => {
      window.removeEventListener("scroll", destectarScroll);
    };
  }, [scroollY]);
  useEffect(() => {}, []);
  useEffect(() => {});
  useEffect(() => {
    return () => {};
  });
  return (
    <>
      <h2>Ciclo de vida con Hooks</h2>
      <p>Scroll Y del navegador {scroollY} px</p>
    </>
  );
}
