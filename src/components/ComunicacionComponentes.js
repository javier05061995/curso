/** @format */

import React, { Component } from "react";

export class Padre extends Component {
  state = {
    contador: 0,
  };
  icrementarContador = () => {
    this.setState({ contador: this.state.contador + 1 });
  };
  render() {
    return (
      <div>
        <h2>componente padre</h2>
        <p>
          <b>Contador: </b>
          {this.state.contador}
        </p>
        <Hijo icrementarContador={this.icrementarContador} mensaje="hijo" />
        <Hijo icrementarContador={this.icrementarContador} mensaje="hijo 2" />
      </div>
    );
  }
}

function Hijo(props) {
  return (
    <>
      <h3>{props.mensaje}</h3>
      <button onClick={props.icrementarContador}>+</button>
    </>
  );
}
