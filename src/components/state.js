/** @format */

import React, { Component } from "react";

function EstadAHijo(props) {
  return (
    <div>
      <h3>
        {props.contadorHijo} {props.hijo}
      </h3>
    </div>
  );
}
export default class State extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contador: 0,
    };

    // setInterval(() => {
    //   this.setState({
    //     contador: this.state.contador + 1,
    //   });
    // }, 1000);
  }
  render() {
    return (
      <>
        <h2>El state</h2>
        <h2>{this.state.contador} padre</h2>
        <EstadAHijo contadorHijo={this.state.contador} hijo="hijo" />
      </>
    );
  }
}
