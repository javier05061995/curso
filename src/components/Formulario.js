/** @format */

import React, { useState } from "react";

// function Formulario(props) {
//   const handleSubmit = (e) => {
//     e.preventDefault();
//     alert("formulario enciado");
//   };
//   const [nombre, setNombre] = useState("");
//   const [sabor, setSabor] = useState("");
//   const [lenguaje, setLenguaje] = useState();
//   const [terminos, setTerminos] = useState();
//   return (
//     <>
//       <h2>Formulario</h2>
//       <form onSubmit={handleSubmit}>
//         <label htmlFor="nombre">Nombre</label>
//         <input
//           type="text"
//           id="nombre"
//           name="nombre"
//           value={nombre}
//           onChange={(e) => setNombre(e.target.value)}
//         />
//         <p>Elije tu Sabor JS favorito: </p>
//         <input
//           type="radio"
//           id="vainilla"
//           name="sabor"
//           value="vainilla"
//           onChange={(e) => setSabor(e.target.value)}
//           defaultChecked
//         />
//         <input
//           type="radio"
//           id="fresa"
//           name="sabor"
//           value="fresa"
//           onChange={(e) => setSabor(e.target.value)}
//         />
//         <input
//           type="radio"
//           id="cambur"
//           name="sabor"
//           value="cambur"
//           onChange={(e) => setSabor(e.target.value)}
//         />
//         <input
//           type="radio"
//           id="pera"
//           name="sabor"
//           value="pera"
//           onChange={(e) => setSabor(e.target.value)}
//         />
//         <label htmlFor="">Sabor Escogido {sabor} </label>

//         <label htmlFor="svelte">Svelte</label>
//         <p>eligeu el lenguaje favorito</p>
//         <select
//           name="lenguaje"
//           onChange={(e) => setLenguaje(e.target.value)}
//           defaultValue=""
//           value={lenguaje}
//         >
//           <option value="">---</option>
//           <option value="js">JS</option>
//           <option value="php">PHP</option>
//           <option value="py">Python</option>
//           <option value="go">GO</option>
//           <option value="rb">RUBY</option>
//         </select>
//         <br />
//         <label htmlFor="terminos">aceptar termino y cone</label>
//         <input
//           type="checkbox"
//           id="terminos"
//           name="terminos"
//           onChange={(e) => setTerminos(e.target.value)}
//           value={terminos}
//         ></input>
//         <br />
//         <input type="submit" />
//       </form>
//     </>
//   );
// }

function Formulario(props) {
  const [form, setForm] = useState({});
  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };
  const handleChecked = (e) => {
    setForm({ ...form, [e.target.name]: e.target.checked });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(form);
  };

  return (
    <>
      <h2>Formulario</h2>
      <form onSubmit={handleSubmit}>
        <label htmlFor="nombre">Nombre</label>
        <input
          type="text"
          id="nombre"
          name="nombre"
          value={form.nombre}
          onChange={handleChange}
        />
        <p>Elije tu Sabor JS favorito: </p>
        <input
          type="radio"
          id="vainilla"
          name="sabor"
          value="vainilla"
          onChange={handleChange}
          defaultChecked
        />
        <input
          type="radio"
          id="fresa"
          name="sabor"
          value="fresa"
          onChange={handleChange}
        />
        <input
          type="radio"
          id="cambur"
          name="sabor"
          value="cambur"
          onChange={handleChange}
        />
        <input
          type="radio"
          id="pera"
          name="sabor"
          value="pera"
          onChange={handleChange}
        />
        <label htmlFor="">Sabor Escogido {form.sabor} </label>

        <label htmlFor="svelte">Svelte</label>
        <p>eligeu el lenguaje favorito</p>
        <select name="lenguaje" onChange={handleChange} defaultValue="">
          <option value="">---</option>
          <option value="js">JS</option>
          <option value="php">PHP</option>
          <option value="py">Python</option>
          <option value="go">GO</option>
          <option value="rb">RUBY</option>
        </select>
        <br />
        <label htmlFor="terminos">aceptar termino y cone</label>
        <input
          type="checkbox"
          id="terminos"
          name="terminos"
          onChange={handleChecked}
        ></input>
        <br />
        <input type="submit" />
      </form>
    </>
  );
}

export default Formulario;
