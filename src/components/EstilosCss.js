import React from "react";
import "./EstilosCss.css";
import moduleStyles from "./Estilos.module.css";

function EstilosCss() {
  let myStyles = {
    borderRadius: "25 rem",
    margin: "2rem auto ",
    maxWidth: "50%",
  };
  return (
    <section className="estilos">
      <h2>Estilos Css en react</h2>
      <h3 className="bg-react">Estilos en hoja CSS externa</h3>
      <h3
        className="bg-react"
        style={{ borderRadius: "25rem", margin: "1rem auto" }}
      >
        Estilos en Linea (artibuto styles)
      </h3>
      <h3 className="bg-react" style={myStyles}>
        Estilos en Linea
      </h3>
      <h3 className="bg-react">
        Agregando NOrmalice CSS
        <br />
        <code>@import-normalice</code>
      </h3>
      <h3 className={moduleStyles.error}>Estilos con modulos Error</h3>
      <h3 className={moduleStyles.success}>Estilos con modulos Success'</h3>
      <h3 className="bg-sass" style={myStyles}>
        Estilos en Linea
      </h3>
    </section>
  );
}

export default EstilosCss;
