/** @format */

import React, { Component } from "react";
import data from "../helpers/data.json";

function ElementLista(props) {
  return (
    <li>
      <a href={props.framework.web} target="_blank" rel="noreferrer">
        Framework {props.framework.name}
      </a>
    </li>
  );
}

export default class RenderizadoElemento extends Component {
  constructor(props) {
    super(props);
    this.state = {
      estaciones: ["Primavera", "Otoño", "Verano", "Invierno"],
    };
  }
  render() {
    return (
      <div>
        <h3>rendenrizando Elemento</h3>
        <h3>estaciones del año</h3>
        <ol>
          {this.state.estaciones.map((el, index) => (
            <li key={index}> {el} </li>
          ))}
        </ol>
        <h3>Framework de JavaScript FRONTEND</h3>
        <ul>
          {data.framework.map((el, index) => (
            <ElementLista framework={el} key={index} />
          ))}
        </ul>
      </div>
    );
  }
}
