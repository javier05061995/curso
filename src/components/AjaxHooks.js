import React, { useState, useEffect } from "react";
function Pokemon(props) {
  return (
    <figure>
      <img src={props.pokemon.avatar} alt={props.pokemon.name}></img>
      <figcaption>{props.pokemon.name}</figcaption>
    </figure>
  );
}

export default function AjaxHook() {
  const [pokemones, setPokemones] = useState([]);
  //   useEffect(() => {
  //     let url = "https://pokeapi.co/api/v2/pokemon/";
  //     fetch(url)
  //       .then((res) => res.json())
  //       .then((json) => {
  //         json.results.forEach((el) => {
  //           fetch(el.url)
  //             .then((res) => res.json())
  //             .then((json2) => {
  //               let pokemon = {
  //                 id: json2.id,
  //                 name: json2.name,
  //                 avatar: json2.sprites.front_default,
  //               };

  //               setPokemones((pokemones) => [...pokemones, pokemon]);
  //             });
  //         });
  //       });
  //   }, []);

  useEffect(() => {
    const getPokemon = async (url) => {
      let repuesta = await fetch(url);
      let json = await repuesta.json();
      json.results.forEach(async (el) => {
        let repuestaHijo = await fetch(el.url);
        let json2 = await repuestaHijo.json();
        let pokemon = {
          id: json2.id,
          name: json2.name,
          avatar: json2.sprites.front_default,
        };

        setPokemones((pokemones) => [...pokemones, pokemon]);
      });
    };

    getPokemon("https://pokeapi.co/api/v2/pokemon/");
  }, []);
  return (
    <>
      <div>peticiones componete Hooks</div>
      {pokemones.map((el) => (
        <Pokemon pokemon={el} key={el.id} />
      ))}
    </>
  );
}
