/** @format */

import React, { Component } from "react";

function Pokemon(props) {
  return (
    <figure>
      <img src={props.pokemon.avatar} alt={props.pokemon.name}></img>
      <figcaption>{props.pokemon.name}</figcaption>
    </figure>
  );
}

class AjaxApi extends Component {
  state = {
    pokemones: [],
  };

  componentDidMount() {
    let url = "https://pokeapi.co/api/v2/pokemon/";
    fetch(url)
      .then((res) => res.json())
      .then((json) => {
        let pokemones = [];
        json.results.forEach((el) => {
          fetch(el.url)
            .then((res) => res.json())
            .then((json2) => {
              pokemones = [
                ...pokemones,
                {
                  id: json2.id,
                  name: json2.name,
                  avatar: json2.sprites.front_default,
                },
              ];
              this.setState({ pokemones }, () => {});
            });
        });
      });
  }
  render() {
    return (
      <>
        <div>peticiones componete class</div>
        {this.state.pokemones.map((el) => (
          <Pokemon pokemon={el} key={el.id} />
        ))}
      </>
    );
  }
}

export default AjaxApi;
