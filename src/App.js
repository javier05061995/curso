/** @format */
import logo from "./logo.svg";

import Componente from "./components/Componente";
import Propiedades from "./components/propiedades";
import RenderizadoCondicional from "./components/RenderizadoCondicional";
import State from "./components/state";
import RenderizadoElemento from "./components/RenderizandoElemento";
import { EventosES6 } from "./components/Eventos";
import { EventosES7 } from "./components/Eventos";
import { MasSobreEventos } from "./components/Eventos";
import { Padre } from "./components/ComunicacionComponentes";

import "./App.css";
import CicloVida from "./components/CicloVida";
import AjaxApi from "./components/AjaxApi";
import ContadorHooks from "./components/Contadorhooks";
import ScrollHooks from "./components/ScrollHooks";
import RelojHooks from "./components/RelojHooks";
import AjaxHook from "./components/AjaxHooks";
import HooksPersonalisado from "./components/HooksPersonalisado";
import Referencias from "./components/Referencias";
import Formulario from "./components/Formulario";
import EstilosCss from "./components/EstilosCss";
import ComponenteEstilisados from "./components/ComponenteEstilisados";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <section>
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </section>
        <section>
          <Componente msg="Hola pazuzu" />
          <hr />
          <Propiedades
            cadena="Esto es una cadena de Texto"
            numero={19}
            boleano={true}
            arreglo={[1, 2, 3]}
            objeto={{ nombre: "javier", correo: "nnyxv123@gmail.com" }}
            funtion={(num) => num * num}
            elementoReact={<i>Esto es un elemento React</i>}
            componenteReact={
              <Componente msg="soy un componente pasado como props" />
            }
          />
          <hr />
          <State />
          <hr />
          <RenderizadoCondicional />
          <hr />
          <RenderizadoElemento />
          <hr />
          <EventosES6 />
          <hr />
          <EventosES7 />
          <hr />
          <MasSobreEventos />
          <hr />
          <Padre />
          <hr />
          <CicloVida />
          <hr />
          <AjaxApi />
          <hr />
          <ContadorHooks titulo="Contador con Hooks" />
          <hr />
          <ScrollHooks />
          <hr />
          <RelojHooks />
          <hr />
          <AjaxHook />
          <hr />
          <HooksPersonalisado />
          <hr />
          <Referencias />
          <hr />
          <Formulario />
          <hr />
          <EstilosCss />
          <hr />
          <ComponenteEstilisados />
          <hr />
        </section>
      </header>
    </div>
  );
}

export default App;
